# client

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

referências

https://gist.github.com/samuelsonbrito/86247fc08c26363ad3988d32fdd05d7d

https://gist.github.com/samuelsonbrito/586862d999d61ebef2a02ae3b18b590b

https://gist.github.com/SoftwareDevPro/17d2d47da429a75c5c655f487502e53b

https://code.tutsplus.com/pt/tutorials/building-restful-apis-with-flask-diy--cms-26625


https://web.digitalinnovation.one/course/desenvolvimento-avancado-de-rest-api-com-flask/learning/88212fb5-0083-43d1-a265-32c173779c29

https://qastack.com.br/programming/13081532/return-json-response-from-flask-view