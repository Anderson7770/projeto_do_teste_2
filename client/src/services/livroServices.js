import  { http } from './configServices'

export default {
    //criando o metodo para listar os métodos
    // com o verbo get() pegando o Livros no 
    // final da url (sufixo)
    listar_:() => {
        return http.get('/')
    },
    add:(Livros) => {
        return http.post('/add_', Livros)
    }
}